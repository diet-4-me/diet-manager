package com.eduardoamaral.dietmanager.models;

import lombok.Data;

@Data
public class Food {
    private String id;
    private String Name;
    private MacroNutrients macroNutrients;
}
