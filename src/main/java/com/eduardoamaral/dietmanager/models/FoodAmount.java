package com.eduardoamaral.dietmanager.models;

import lombok.Data;

@Data
public class FoodAmount {
    private double grams;
    private Food food;
}
