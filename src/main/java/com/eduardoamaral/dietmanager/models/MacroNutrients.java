package com.eduardoamaral.dietmanager.models;

import lombok.Data;

@Data
public class MacroNutrients {
    private String carbohydrate;
    private String fat;
    private String protein;
    private String calories;
}
