package com.eduardoamaral.dietmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class DietManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DietManagerApplication.class, args);
    }

}
