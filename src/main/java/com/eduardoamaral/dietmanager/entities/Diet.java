package com.eduardoamaral.dietmanager.entities;

import com.eduardoamaral.dietmanager.models.MacroNutrients;
import lombok.Data;

import java.util.List;

@Data
public class Diet {
    private String id;
    private String name;
    private List<Meal> meals;
    private MacroNutrients macroNutrients;
}
