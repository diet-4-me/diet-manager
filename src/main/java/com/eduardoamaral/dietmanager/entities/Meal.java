package com.eduardoamaral.dietmanager.entities;

import com.eduardoamaral.dietmanager.models.FoodAmount;
import com.eduardoamaral.dietmanager.models.MacroNutrients;
import lombok.Data;

import java.util.List;

@Data
public class Meal {
    private String id;
    private List<FoodAmount> foods;
    private MacroNutrients macroNutrients;
}
