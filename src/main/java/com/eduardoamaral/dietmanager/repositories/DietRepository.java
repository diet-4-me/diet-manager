package com.eduardoamaral.dietmanager.repositories;

import com.eduardoamaral.dietmanager.entities.Diet;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DietRepository extends MongoRepository<Diet, String> {
}
