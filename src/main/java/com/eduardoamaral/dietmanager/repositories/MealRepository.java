package com.eduardoamaral.dietmanager.repositories;

import com.eduardoamaral.dietmanager.entities.Meal;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MealRepository extends MongoRepository<Meal, String> {
}
